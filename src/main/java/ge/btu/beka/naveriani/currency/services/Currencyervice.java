package ge.btu.beka.naveriani.currency.services;

import ge.btu.beka.naveriani.currency.models.Currency;

import java.util.List;

public interface Currencyervice {

    String getCurrency(String cur);

    String getCurrencyDescription(String cur);

    String getDate();
}
