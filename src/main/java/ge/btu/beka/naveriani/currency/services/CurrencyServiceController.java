package ge.btu.beka.naveriani.currency.services;

import ge.btu.beka.naveriani.currency.models.Currency;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class CurrencyServiceController {

    private Currencyervice service = CurrencyServiceImpl.getInstance();

    @WebMethod
    public String getCurrency(@WebParam(name = "currency") String cur){
        return service.getCurrency(cur);
    }

    @WebMethod
    public String getCurrencyDescription(@WebParam(name = "currency") String cur){
        return service.getCurrencyDescription(cur);
    }

    @WebMethod
    public String getDate(){
        return service.getDate();
    }


}
