package ge.btu.beka.naveriani.currency.models;

import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement
public class Currency {

    private BigDecimal value;

    private String ticker;

    private String description;

    public Currency(BigDecimal value, String ticker, String description){
        this.value = value;
        this.ticker = ticker;
        this.description = description;
    }

    public Currency(){

    }

    public BigDecimal getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public String getTicker() {
        return ticker;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
