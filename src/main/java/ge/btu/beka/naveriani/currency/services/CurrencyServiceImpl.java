package ge.btu.beka.naveriani.currency.services;

import ge.btu.beka.naveriani.currency.models.Currency;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CurrencyServiceImpl implements Currencyervice {

    private static Currencyervice currencyervice;

    private  Map<String, Currency> currencies = new HashMap<String, Currency>();

    CurrencyServiceImpl(){
        currencies.put("USD", new Currency(BigDecimal.valueOf(3.14), "USD", "1 USD to 1 GEl"));
        currencies.put("GBP", new Currency(BigDecimal.valueOf(4), "GBP", "1 GBP to 1 GEl"));
        currencies.put("RUB", new Currency(BigDecimal.valueOf(0.5), "RUB", "100 RUB to 1 GEl"));
    }


    public static Currencyervice getInstance(){
        if (currencyervice == null){
            currencyervice = new CurrencyServiceImpl();
        }
        return currencyervice;
    }

    public String getCurrency(String cur) {
        return currencies.get(cur.toUpperCase()).getValue().toPlainString();
    }

    public String getCurrencyDescription(String cur) {
        return currencies.get(cur.toUpperCase()).getDescription();
    }

    public String getDate() {
        return new Date().toString();
    }
}
